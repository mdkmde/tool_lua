/*
https://gitlab.com/mdkmde/tool_lua/ - matthias (at) koerpermagie.de

Copyright (c) MIT-License
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <lua.h>
#include <lauxlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <uuid/uuid.h>
#define CRYPT_NONE 0
#define CRYPT_GCRYPT 1
#define CRYPT_OPENSSL 2
#if defined GCRYPT
 #define CRYPT_LIB CRYPT_GCRYPT
 #undef OPENSSL
 #include <gcrypt.h>
#elif defined OPENSSL
 #define CRYPT_LIB CRYPT_OPENSSL
 #include <openssl/evp.h>
 #include <openssl/hmac.h>
#else
 #define CRYPT_LIB CRYPT_NONE
#endif
#if defined READLINE
#include <readline/readline.h>
#include <readline/history.h>
#endif
#include <termios.h>
#include <pwd.h>

#define _FILENAME "tool.so"
#define _AUTHOR "Matthias Diener"
#define _VERSION "20170629"

static int l_getVersion(lua_State *L) {
  lua_pushstring(L, _VERSION);
  lua_pushstring(L, _AUTHOR);
  lua_pushstring(L, _FILENAME);
  return 3;
}

static int l_getPid(lua_State *L) {
  lua_pushinteger(L, getpid());
  return 1;
}

static int l_sleep(lua_State *L) {
  unsigned int time = 1;
  if (lua_isnumber(L, 1)) {
    time = luaL_checkinteger(L, 1);
    if (time < 1) {
      time = 1;
    }
  }
  sleep(time);
  return 0;
}

static int l_msleep(lua_State *L) {
  struct timespec ts;
  unsigned long mtime = 1;
  if (lua_isnumber(L, 1)) {
    mtime = luaL_checkinteger(L, 1);
    if (mtime < 1) {
      mtime = 1;
    }
  }
  ts.tv_sec = (int)(mtime / 1000);
  ts.tv_nsec = (long)((mtime % 1000) * 1000000);
  nanosleep(&ts, NULL);
  return 0;
}

static int l_unlink(lua_State *L) {
  const char *name = luaL_checkstring(L, 1);
  if (unlink(name) == 0) {
    lua_pushboolean(L, 1);
    return 1;
  }
  lua_pushnil(L);
  lua_pushfstring(L, "%s", strerror(errno));
  return 2;
}

static int l_fileExists(lua_State *L) {
  struct stat fstat;
  const char *name = luaL_checkstring(L, 1);
  if (stat(name, &fstat) == 0) {
    if (S_ISBLK(fstat.st_mode) != 0) {
      lua_pushinteger(L, (lua_Integer)1);
    } else if (S_ISCHR(fstat.st_mode) != 0) {
      lua_pushinteger(L, (lua_Integer)2);
    } else if (S_ISDIR(fstat.st_mode) != 0) {
      lua_pushinteger(L, (lua_Integer)3);
    } else if (S_ISFIFO(fstat.st_mode) != 0) {
      lua_pushinteger(L, (lua_Integer)4);
    } else if (S_ISREG(fstat.st_mode) != 0) {
      lua_pushinteger(L, (lua_Integer)5);
    } else if (S_ISLNK(fstat.st_mode) != 0) {
      lua_pushinteger(L, (lua_Integer)6);
    } else if (S_ISSOCK(fstat.st_mode) != 0) {
      lua_pushinteger(L, (lua_Integer)7);
    } else {
      /* unknown file type - should not happen */
      lua_pushinteger(L, (lua_Integer)8);
    }
    return 1;
  }
  lua_pushnil(L);
  lua_pushfstring(L, "%s", strerror(errno));
  return 2;
}

static int l_mkdir(lua_State *L) {
  const char *path = luaL_checkstring(L, 1);
  if (mkdir(path, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP |
                  S_IWGRP | S_IXGRP | S_IROTH | S_IXOTH ) == 0) {
    lua_pushboolean(L, 1);
    return 1;
  }
  lua_pushnil(L);
  lua_pushfstring(L, "%s", strerror(errno));
  return 2;
}

static int l_rmdir(lua_State *L) {
  const char *path = luaL_checkstring(L, 1);
  if (rmdir(path) == 0) {
    lua_pushboolean(L, 1);
    return 1;
  }
  lua_pushnil(L);
  lua_pushfstring(L, "%s", strerror(errno));
  return 2;
}

static int l_getUuid(lua_State *L) {
  const char *type = luaL_optstring(L, 1, NULL);
  /* 36-byte string (plus tailing '\0') */
  char s_uuid[(36 + 1)];
  uuid_t uuid;
  if (type && *type == '*')
    if (*(type + 1) == 't')
      uuid_generate_time(uuid);
    else if (*(type + 1) == 'r')
      uuid_generate_random(uuid);
    else
      uuid_generate(uuid);
  else
    uuid_generate(uuid);
  uuid_unparse(uuid, s_uuid);
  lua_pushfstring(L, "%s", s_uuid);
  return 1;
}

static int l_getUsec(lua_State *L) {
  struct timeval microtime;
  gettimeofday(&microtime, NULL);
  lua_pushinteger(L, (lua_Integer)microtime.tv_usec);
  return 1;
}

/* BEGIN extract heimdal/lib/roken */

/*
 * Copyright (c) 1995-2001 Kungliga Tekniska Högskolan
 * (Royal Institute of Technology, Stockholm, Sweden).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


static const char base64_chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static int pos(char c) {
  const char *p;

  for (p = base64_chars; *p; p++)
    if (*p == c)
      return p - base64_chars;
  return -1;
}

int base64_encode(const void *data, int size, unsigned char **str) {
  char *s, *p;
  int i;
  int c;
  const unsigned char *q;

  if (size > INT_MAX/4 || size < 0) {
    *str = NULL;
    return -1;
  }
  p = s = (char *) malloc(size * 4 / 3 + 4);
  if (p == NULL) {
    *str = NULL;
    return -1;
  }
  q = (const unsigned char *) data;
  for (i = 0; i < size;) {
    c = q[i++];
    c *= 256;
    if (i < size)
      c += q[i];
    i++;
    c *= 256;
    if (i < size)
      c += q[i];
    i++;
    p[0] = base64_chars[(c & 0x00fc0000) >> 18];
    p[1] = base64_chars[(c & 0x0003f000) >> 12];
    p[2] = base64_chars[(c & 0x00000fc0) >> 6];
    p[3] = base64_chars[(c & 0x0000003f) >> 0];
    if (i > size)
      p[3] = '=';
    if (i > size + 1)
      p[2] = '=';
    p += 4;
  }
  *p = 0;
  *str = (unsigned char *)s;
  return (int) strlen(s);
}

#define DECODE_ERROR 0xffffffff

static unsigned int token_decode(const char *token) {
  int i;
  unsigned int val = 0;
  int marker = 0;

  if (strlen(token) < 4)
    return DECODE_ERROR;
  for (i = 0; i < 4; i++) {
    val *= 64;
    if (token[i] == '=')
      marker++;
    else if (marker > 0)
      return DECODE_ERROR;
    else
      val += pos(token[i]);
  }
  if (marker > 2)
    return DECODE_ERROR;
  return (marker << 24) | val;
}

int base64_decode(const char *str, void *data) {
  const char *p;
  unsigned char *q;

  q = data;
  for (p = str; *p && (*p == '=' || strchr(base64_chars, *p)); p += 4) {
    unsigned int val = token_decode(p);
    unsigned int marker = (val >> 24) & 0xff;

    if (val == DECODE_ERROR)
      return -1;
    *q++ = (val >> 16) & 0xff;
    if (marker < 2)
      *q++ = (val >> 8) & 0xff;
    if (marker < 1)
      *q++ = val & 0xff;
  }
  return q - (unsigned char *) data;
}

/* END extract heimdal/lib/roken */

static int l_getCryptVersion(lua_State *L) {
  lua_pushinteger(L, CRYPT_LIB);
  return 1;
}

#define BASE64_LEN(n) (((n + 2) / 3) * 4)
#if defined GCRYPT || defined OPENSSL

static int push_enc_string(lua_State *L, const unsigned char *is_buf, size_t n_is_buf_len, int n_out_codec) {
  const char *out_codec = luaL_optstring(L, n_out_codec, " ");
  char *os_buf;
  unsigned int i;
  char *s_hex;
  unsigned int n_os_buf_len;

  os_buf = 0;
  if ((out_codec[0] == 'x') || (out_codec[0] == 'X')) {
    if (!(os_buf = (char *)calloc(n_is_buf_len * 2 + 1, sizeof(char)))) {
      lua_pushnil(L);
      lua_pushfstring(L, "%s", "cannot allocate memory");
      return 2;
    }
    if (out_codec[0] == 'x') { s_hex = "%02x"; } else { s_hex = "%02X"; }
    for (i = 0; i < n_is_buf_len; i++) {
      sprintf(&os_buf[i * 2], s_hex, is_buf[i]);
    }
    os_buf[n_is_buf_len * 2 + 1] = 0;
    lua_pushlstring(L, os_buf, n_is_buf_len * 2);
  } else if (out_codec[0] == 'b') {
    if (!(os_buf = (char *)calloc((BASE64_LEN(n_is_buf_len)) + 1, sizeof(char)))) {
      lua_pushnil(L);
      lua_pushfstring(L, "%s", "cannot allocate memory");
      return 2;
    }
    n_os_buf_len = base64_encode(is_buf, n_is_buf_len, (unsigned char **)&os_buf);
    os_buf[n_os_buf_len + 1] = 0;
    lua_pushlstring(L, os_buf, n_os_buf_len);
  } else {
    lua_pushlstring(L, (char *)is_buf, n_is_buf_len);
  }
  if (os_buf) free(os_buf);
  return 1;
}

static int l_hash(lua_State *L) {
  const char *is_buf = luaL_checkstring(L, 1);
  const char *is_algo = luaL_checkstring(L, 2);
  int result;
#ifdef GCRYPT
  unsigned int n_algo = gcry_md_map_name(is_algo);
  const char *is_hmac_key;
  unsigned int md_flag = 0;
  unsigned char *s_digest;
  size_t n_algo_dlen = gcry_md_get_algo_dlen(n_algo);
  gcry_md_hd_t md_hd;

  if (n_algo == 0) {
    lua_pushnil(L);
    lua_pushstring(L, "invalid digest algorithm identifier");
    return 2;
  }
  if (lua_type(L, 3) == LUA_TSTRING) {
    md_flag = GCRY_MD_FLAG_HMAC;
    is_hmac_key = luaL_checkstring(L, 3);
  }
  gcry_md_open(&md_hd, n_algo, md_flag);
  if (md_flag) {
    gcry_md_setkey(md_hd, is_hmac_key, luaL_len(L, 3));
  }
  gcry_md_write(md_hd, is_buf, luaL_len(L, 1));
  s_digest = gcry_md_read(md_hd, n_algo);
  result = push_enc_string(L, s_digest, n_algo_dlen, 4);
  gcry_md_close(md_hd);
#endif
#ifdef OPENSSL
  EVP_MD_CTX mdctx;
  HMAC_CTX hmacctx;
  const EVP_MD *md = EVP_get_digestbyname(is_algo);
  const unsigned char *is_hmac_key;
  unsigned char s_digest[EVP_MAX_MD_SIZE];
  unsigned int n_algo_dlen;

  if (!md) {
    lua_pushnil(L);
    lua_pushfstring(L, "%s", "invalid digest algorithm identifier");
    return 2;
  }
  if (lua_type(L, 3) == LUA_TSTRING) {
    is_hmac_key = (unsigned char *)luaL_checkstring(L, 3);
    HMAC_CTX_init(&hmacctx);
    HMAC_Init_ex(&hmacctx, is_hmac_key, luaL_len(L, 3), md, NULL);
    HMAC_Update(&hmacctx, (unsigned char *)is_buf, luaL_len(L, 1));
    HMAC_Final(&hmacctx, s_digest, &n_algo_dlen);
    result = push_enc_string(L, s_digest, n_algo_dlen, 4);
    HMAC_CTX_cleanup(&hmacctx);
  } else {
    EVP_MD_CTX_init(&mdctx);
    EVP_DigestInit_ex(&mdctx, md, NULL);
    EVP_DigestUpdate(&mdctx, is_buf, luaL_len(L, 1));
    EVP_DigestFinal_ex(&mdctx, s_digest, &n_algo_dlen);
    result = push_enc_string(L, s_digest, n_algo_dlen, 4);
    EVP_MD_CTX_cleanup(&mdctx);
  }
#endif
  return result;
}

#ifdef GCRYPT
 #define CIPHER_MD_ALGO GCRY_MD_MD5
#endif
#ifdef OPENSSL
 #define CIPHER_MD_ALGO EVP_md5()
#endif
#define ENCRYPT 0
#define DECRYPT 1
#define CIPHER_KEYLEN 16
#define CIPHER_IV "in1tIal+vEct0r16"

static int l_crypt(lua_State *L) {
  size_t n_is_buf_len;
  const char *is_buf = luaL_checklstring(L, 1, &n_is_buf_len);
  const char *is_pass = luaL_checkstring(L, 2);
  unsigned int i, result;
#ifdef GCRYPT
  size_t n_s_buf_len;
  unsigned char *s_buf;
  unsigned char *s_digest;
  gcry_md_hd_t md_hd;
  gcry_cipher_hd_t cipher_hd;

  gcry_md_open(&md_hd, CIPHER_MD_ALGO, 0);
  gcry_md_write(md_hd, is_pass, luaL_len(L, 2));
  s_digest = gcry_md_read(md_hd, CIPHER_MD_ALGO);
  gcry_cipher_open(&cipher_hd, GCRY_CIPHER_TWOFISH, GCRY_CIPHER_MODE_CBC, 0);
  gcry_cipher_setkey(cipher_hd, s_digest, CIPHER_KEYLEN);
  gcry_cipher_setiv(cipher_hd, CIPHER_IV, CIPHER_KEYLEN);
  s_buf = 0;
  if (lua_toboolean(L, 3) == DECRYPT) {
    const char *in_codec = luaL_optstring(L, 4, " ");
    char *s_hex;

    if (!(s_buf = (unsigned char *)gcry_calloc(n_is_buf_len + 1, sizeof(unsigned char)))) {
      lua_pushnil(L);
      lua_pushfstring(L, "%s", "cannot allocate memory");
      return 2;
    }
    if ((in_codec[0] == 'x') || (in_codec[0] == 'X')) {
      if (in_codec[0] == 'x') { s_hex = "%02x"; } else { s_hex = "%02X"; }
      n_is_buf_len = n_is_buf_len / 2;
      for (i = 0; i < n_is_buf_len; i++) {
        sscanf(&is_buf[i * 2], s_hex, &s_buf[i]);
      }
      s_buf[n_is_buf_len + 1] = 0;
      gcry_cipher_decrypt(cipher_hd, s_buf, n_is_buf_len, NULL, 0);
    } else if (in_codec[0] == 'b') {
      n_is_buf_len = base64_decode(is_buf, s_buf);
      s_buf[n_is_buf_len + 1] = 0;
      gcry_cipher_decrypt(cipher_hd, s_buf, n_is_buf_len, NULL, 0);
    } else {
      gcry_cipher_decrypt(cipher_hd, s_buf, n_is_buf_len, is_buf, n_is_buf_len);
    }
    s_buf[n_is_buf_len + 1] = 0;
    for (n_s_buf_len = 0; s_buf[n_s_buf_len] != 0; n_s_buf_len++);
    lua_pushlstring(L, (char *)s_buf, n_s_buf_len);
    result = 1;
  } else {
    for (n_s_buf_len = CIPHER_KEYLEN; n_s_buf_len < n_is_buf_len; n_s_buf_len += CIPHER_KEYLEN);
    if (!(s_buf = (unsigned char *)gcry_calloc(n_s_buf_len + 1, sizeof(unsigned char)))) {
      lua_pushnil(L);
      lua_pushfstring(L, "%s", "cannot allocate memory");
      return 2;
    }
    for (i = 0; i < n_is_buf_len; i++) {
      s_buf[i] = is_buf[i];
    }
    for(i = n_is_buf_len; i < (n_s_buf_len + 1); i++) {s_buf[i] = 0;}
    gcry_cipher_encrypt(cipher_hd, s_buf, n_s_buf_len, NULL, 0);
    s_buf[n_s_buf_len + 1] = 0;
    result = push_enc_string(L, s_buf, n_s_buf_len, 4);
  }
  if (s_buf) gcry_free(s_buf);
  gcry_cipher_close(cipher_hd);
  gcry_md_close(md_hd);
#endif
#ifdef OPENSSL
  EVP_CIPHER_CTX ctx;
  EVP_MD_CTX mdctx;
  int n_s_buf_len;
  unsigned char *s_buf;
  unsigned char s_digest[EVP_MAX_MD_SIZE];

  EVP_CIPHER_CTX_init(&ctx);
  EVP_MD_CTX_init(&mdctx);
  EVP_DigestInit_ex(&mdctx, CIPHER_MD_ALGO, NULL);
  EVP_DigestUpdate(&mdctx, is_pass, luaL_len(L, 2));
  EVP_DigestFinal_ex(&mdctx, s_digest, NULL);
  if (!(s_buf = (unsigned char *)calloc(n_s_buf_len + EVP_MAX_BLOCK_LENGTH, sizeof(unsigned char)))) {
    lua_pushnil(L);
    lua_pushfstring(L, "%s", "cannot allocate memory");
    return 2;
  }
  if (lua_toboolean(L, 3) == DECRYPT) {
    const char *in_codec = luaL_optstring(L, 4, " ");
    char *s_hex;

    /* decryption == 0 */
    EVP_CipherInit_ex(&ctx, EVP_bf_cbc(), NULL, s_digest, (unsigned char *)CIPHER_IV, 0);
    if ((in_codec[0] == 'x') || (in_codec[0] == 'X')) {
      if (in_codec[0] == 'x') { s_hex = "%02x"; } else { s_hex = "%02X"; }
      n_is_buf_len = n_is_buf_len / 2;
      for (i = 0; i < n_is_buf_len; i++) {
        sscanf(&is_buf[i * 2], s_hex, &s_buf[i]);
      }
      s_buf[n_is_buf_len + 1] = 0;
      EVP_CipherUpdate(&ctx, s_buf, &n_s_buf_len, s_buf, n_is_buf_len);
    } else if (in_codec[0] == 'b') {
      n_is_buf_len = base64_decode(is_buf, s_buf);
      s_buf[n_is_buf_len + 1] = 0;
      EVP_CipherUpdate(&ctx, s_buf, &n_s_buf_len, s_buf, n_is_buf_len);
    } else {
      EVP_CipherUpdate(&ctx, s_buf, &n_s_buf_len, (unsigned char *)is_buf, n_is_buf_len);
    }
    EVP_CipherFinal_ex(&ctx, s_buf, &n_s_buf_len);
    lua_pushlstring(L, (char *)s_buf, n_s_buf_len);
    result = 1;
  } else {
    /* encryption == 1 */
    EVP_CipherInit_ex(&ctx, EVP_bf_cbc(), NULL, s_digest, (unsigned char *)CIPHER_IV, 1);
    EVP_CipherUpdate(&ctx, s_buf, &n_s_buf_len, (unsigned char *)is_buf, n_is_buf_len);
    EVP_CipherFinal_ex(&ctx, s_buf, &n_s_buf_len);
    result = push_enc_string(L, s_buf, n_s_buf_len, 4);
  }
  free(s_buf);
  EVP_MD_CTX_cleanup(&mdctx);
  EVP_CIPHER_CTX_cleanup(&ctx);
#endif
  return result;
}

#endif

#define ENCODE 0
#define DECODE 1

static int l_b64(lua_State *L) {
  size_t n_buf_len;
  const char *is_buf = luaL_checklstring(L, 1, &n_buf_len);
  char *s_buf;

  s_buf = 0;
  if (lua_toboolean(L, 2) == DECODE) {
    if (!(s_buf = (char *)calloc(n_buf_len, sizeof(char)))) {
      lua_pushnil(L);
      lua_pushfstring(L, "%s", "cannot allocate memory");
      return 2;
    }
    n_buf_len = base64_decode(is_buf, s_buf);
  } else {
    if (!(s_buf = (char *)calloc((BASE64_LEN(n_buf_len)) + 1, sizeof(char)))) {
      lua_pushnil(L);
      lua_pushfstring(L, "%s", "cannot allocate memory");
      return 2;
    }
    n_buf_len = base64_encode(is_buf, n_buf_len, (unsigned char **)&s_buf);
  }
  s_buf[n_buf_len + 1] = 0;
  lua_pushlstring(L, s_buf, n_buf_len);
  free(s_buf);
  return 1;
}

#if defined READLINE
static lua_State *globalL;

static char* tableIterator(const char* text, int state) {
  size_t len;
  const char* str;
  char* result;
  lua_State* L = globalL;

  lua_rawgeti(L, -1, state + 1);
  if (lua_isnil(L, -1)) {
    return NULL;
  }
  str = lua_tolstring(L, -1, &len);
  result = (char *)malloc(len + 1);
  strcpy(result, str);
  lua_pop(L, 1);
  return result;
}

static char** doCompletion(const char * text, int start, int end) {
  char **matches = NULL;
  lua_State *L = globalL;
  int top;

  if (L == NULL) {
    return NULL;
  }
  top = lua_gettop(L);
  lua_getglobal(L, "package");
  lua_getfield(L, -1, "loaded");
  lua_remove(L, -2);
  lua_getfield(L, -1, "tool");
  lua_remove(L, -2);
  lua_getfield(L, -1, "doCompletion");
  lua_remove(L, -2);
  if (!lua_isfunction(L, -1)) {
    lua_settop(L, top);
    return NULL;
  }
  lua_pushstring(L, text);
  lua_pushstring(L, rl_line_buffer);
  lua_pushinteger(L, start + 1);
  lua_pushinteger(L, end + 1);
  if ((lua_pcall(L, 4, 1, 0)) || (!lua_istable(L, -1))) {
    lua_settop(L, top);
    return NULL;
  }
  matches = rl_completion_matches(text, tableIterator);
  lua_settop(L, top);
  return matches;
}

static int l_readline(lua_State *L) {
  const char *word = lua_tostring(L, 1);
  char *line = readline(word);
  lua_pushstring(L, line);
  free(line);
  return 1;
}

static int l_addHistory(lua_State *L) {
  if (luaL_len(L, 1) > 0) {
    add_history(lua_tostring(L, 1));
  }
  return 0;
}
#endif

static int l_getPass(lua_State *L) {
  const char *is_prompt = luaL_optstring(L, 1, NULL);
  struct termios oflags, nflags;
  char s_password[255];

  /* disabling echo */
  tcgetattr(fileno(stdin), &oflags);
  nflags = oflags;
  nflags.c_lflag &= ~ECHO;
  nflags.c_lflag |= ECHONL;

  if (tcsetattr(fileno(stdin), TCSANOW, &nflags) != 0) {
    lua_pushnil(L);
    lua_pushfstring(L, "%s", strerror(errno));
    return 2;
  }

  if (is_prompt) {
    printf(is_prompt);
  }
  fgets(s_password, sizeof(s_password), stdin);
  s_password[strlen(s_password) - 1] = 0;

  /* restore terminal */
  if (tcsetattr(fileno(stdin), TCSANOW, &oflags) != 0) {
    lua_pushnil(L);
    lua_pushfstring(L, "%s", strerror(errno));
    return 2;
  }

  lua_pushstring(L, s_password);
  return 1;
}

static int l_chuser(lua_State *L) {
  struct passwd *pw = NULL;
  const char *user = luaL_optstring(L, 1, NULL);
  int result = 1;

  if((pw = getpwnam(user)) == NULL) {
    lua_pushnil(L);
    lua_pushfstring(L, "User: \"%s\" does not exist.", user);
    result = 2;
  } else if(geteuid() != 0) {
    if((geteuid() == pw->pw_uid) && (getegid() == pw->pw_gid)) {
      lua_pushboolean(L, 1);
      result = 1;
    } else {
      lua_pushnil(L);
      lua_pushstring(L, "Only root can do that.");
      result = 2;
    }
  } else if((setgid(pw->pw_gid) == -1) || (setuid(pw->pw_uid) == -1)) {
    lua_pushnil(L);
    lua_pushstring(L, "Cannot drop privileges.");
    result = 2;
  } else {
    lua_pushboolean(L, 1);
    result = 1;
  }
  endpwent();
  return result;
}

static int l_bg(lua_State *L) {
  int pid;

  if((pid = fork()) == -1) {
    lua_pushnil(L);
    lua_pushstring(L, "Cannot fork() to background.");
    return 2;
  } else if(pid > 0) { /* parent - not needed */
    exit(0);
  }
  lua_pushinteger(L, (lua_Integer)getpid());
  return 1;
}

static const struct luaL_Reg tool[] = {
  {"getVersion", l_getVersion},
  {"getPid", l_getPid},
  {"sleep", l_sleep},
  {"msleep", l_msleep},
  {"unlink", l_unlink},
  {"mkdir", l_mkdir},
  {"rmdir", l_rmdir},
  {"fileExists", l_fileExists},
  {"getUuid", l_getUuid},
  {"getUsec", l_getUsec},
  {"getCryptVersion", l_getCryptVersion},
#if defined GCRYPT || defined OPENSSL
  {"hash", l_hash},
  {"crypt", l_crypt},
#endif
  {"b64", l_b64},
#if defined READLINE
  {"readline", l_readline},
  {"addHistory", l_addHistory},
#endif
  {"getPass", l_getPass},
  {"chuser", l_chuser},
  {"bg", l_bg},
  {NULL, NULL}
};

int luaopen_tool(lua_State *L) {
#ifdef GCRYPT
  gcry_check_version(GCRYPT_VERSION);
#endif
#ifdef OPENSSL
  OpenSSL_add_all_algorithms();
#endif
  luaL_newlib(L, tool);
#if defined GCRYPT || defined OPENSSL
  lua_pushstring(L, "ENCRYPT");
  lua_pushboolean(L, ENCRYPT);
  lua_rawset(L, -3);
  lua_pushstring(L, "DECRYPT");
  lua_pushboolean(L, DECRYPT);
  lua_rawset(L, -3);
#endif
  lua_pushstring(L, "CRYPT_NONE");
  lua_pushinteger(L, CRYPT_NONE);
  lua_rawset(L, -3);
  lua_pushstring(L, "CRYPT_GCRYPT");
  lua_pushinteger(L, CRYPT_GCRYPT);
  lua_rawset(L, -3);
  lua_pushstring(L, "CRYPT_OPENSSL");
  lua_pushinteger(L, CRYPT_OPENSSL);
  lua_rawset(L, -3);
  lua_pushstring(L, "ENCODE");
  lua_pushboolean(L, ENCODE);
  lua_rawset(L, -3);
  lua_pushstring(L, "DECODE");
  lua_pushboolean(L, DECODE);
  lua_rawset(L, -3);
  lua_pushstring(L, "TYPE_BLOCK");
  lua_pushinteger(L, 1);
  lua_rawset(L, -3);
  lua_pushstring(L, "TYPE_CHAR");
  lua_pushinteger(L, 2);
  lua_rawset(L, -3);
  lua_pushstring(L, "TYPE_DIR");
  lua_pushinteger(L, 3);
  lua_rawset(L, -3);
  lua_pushstring(L, "TYPE_FIFO");
  lua_pushinteger(L, 4);
  lua_rawset(L, -3);
  lua_pushstring(L, "TYPE_FILE");
  lua_pushinteger(L, 5);
  lua_rawset(L, -3);
  lua_pushstring(L, "TYPE_REG");
  lua_pushinteger(L, 6);
  lua_rawset(L, -3);
  lua_pushstring(L, "TYPE_SOCK");
  lua_pushinteger(L, 7);
  lua_rawset(L, -3);
  lua_pushstring(L, "TYPE_UNKNOWN");
  lua_pushinteger(L, 8);
  lua_rawset(L, -3);
#if defined READLINE
  globalL = L;
  rl_attempted_completion_function = doCompletion;
  rl_completion_append_character = '\0';
#endif
  return 1;
}

