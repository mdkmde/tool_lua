LUA_VERSION=5.3
CRYPT_METHOD="gcrypt"
CC="gcc -std=gnu99"
CC="tcc"
CFLAGS="-shared -fPIC -O2 -pedantic -Wall"
COMMON_LIBS="-I/usr/include/lua${LUA_VERSION} -llua${LUA_VERSION} -luuid"
COMMON_LIBS="-I/usr/include/lua${LUA_VERSION} -L/usr/lib/lua${LUA_VERSION} -llua -luuid"
CFLAGS="${CFLAGS} -DREADLINE"
COMMON_LIBS="${COMMON_LIBS} -lreadline"

case "${1:-${CRYPT_METHOD}}" in
  nocrypt)
    :
  ;;
  ssl|openssl)
    CFLAGS="${CFLAGS} -DOPENSSL"
    COMMON_LIBS="${COMMON_LIBS} -lssl"
  ;;
  gcrypt)
    CFLAGS="${CFLAGS} -DGCRYPT"
    COMMON_LIBS="${COMMON_LIBS} -lgcrypt"
  ;;
  *)
    echo "Usage: ${0} [nocrypt|ssl|opensst|gcrypt]"
    echo "Default: ${CRYPT_METHOD}"
    exit 1
  ;;
esac

eval ${CC} ${CFLAGS} -o tool.so ${COMMON_LIBS} tool.c

